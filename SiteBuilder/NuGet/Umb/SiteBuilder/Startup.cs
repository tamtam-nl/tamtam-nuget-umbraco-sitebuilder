﻿using System.IO;
using Umbraco.Core;
using System;
using System.Web;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public class Startup : IApplicationEventHandler
    {

        private static FileSystemWatcher _fsw;
        private static object _syncObject = new object();

        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            try
            {
                //Initialization
                Init();

                //Manager routing
                //RouteTable.Routes.MapConnection<Manager>("sitebuilder.manager", "/sitebuilder/manager");

                //Set monitoring on config file
                var path = Path.Combine(HttpRuntime.AppDomainAppPath, "config");
                var file = "TamTam.Nuget.Umb.SiteBuilder.config";
                _fsw = new FileSystemWatcher(path, file);
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fsw.Changed += _fsw_Changed;
                _fsw.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void Init(bool reInit = false)
        {
            DataTypes.Initialize(reInit);
            DocTypes.Initialize(reInit);
            Templates.Initialize(reInit);
            Macros.Initialize(reInit);
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Nothing
        }

        static void _fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                lock (_syncObject)
                {
                    Settings.DataTypes.ReadSettings(e.FullPath);
                    Settings.Macros.ReadSettings(e.FullPath);
                    Settings.Templates.ReadSettings(e.FullPath);
                    Settings.DocTypes.ReadSettings(e.FullPath);
                    Init(true);
                }
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }


    }
}