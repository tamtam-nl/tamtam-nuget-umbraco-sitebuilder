﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class Worker
    {
        private static Queue<Task> _queue = new Queue<Task>();
        private static List<Task> _done = new List<Task>();
        private static Thread _queuesThread = null;
        public delegate object ProcessFileDelegate(string file, Monitoring.LogEntry logEntr, bool test);

        public static bool Queue(string key, ProcessFileDelegate method, Monitoring.LogEntry logEntry, bool test)
        {
            if (!_queue.Any(t => t.Key == key) && !_done.Any(t => t.Key == key))
            {
                _queue.Enqueue(new Task(key, method, logEntry, test));
            }
            StartProcessQueue();
            return _done.Any(t => t.Key == key);
        }

        private static void StartProcessQueue()
        {
            if (_queuesThread == null || _queuesThread.ThreadState != ThreadState.Running)
            {
                _queuesThread = new Thread(new ThreadStart(ProcessQueue));
                _queuesThread.Start();
            }
        }

        private static void ProcessQueue()
        {
            while (_queue.Count > 0)
            {
                var task = _queue.Dequeue();
                var result = task.Method.Invoke(task.Key, task.LogEntry, task.Test);
                if (result != null)
                {
                    _done.Add(task);
                }
                //_queue.Dequeue();
            }
            _done.Clear();
        }

        private class Task
        {
            public Guid Sequence { get; private set; }
            public string Key { get; private set; }
            public ProcessFileDelegate Method { get; private set; }
            public Monitoring.LogEntry LogEntry { get; private set; }
            public bool Test { get; set; }
            public Task(string file, ProcessFileDelegate method, Monitoring.LogEntry logEntry, bool test)
            {
                this.Key = file;
                this.Method = method;
                this.LogEntry = logEntry;
                this.Test = test;
            }
        }
    }
}