﻿using System.IO;
using Umbraco.Core;
using umbraco.cms.businesslogic.macro;
using System.Collections;
using System.Linq;
using System.Web.Hosting;
using System;
using Umbraco.Core.Models;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class Macros
    {
        private static string _basePath = "/Views/MacroPartials";
        private static string _baseDir = HostingEnvironment.MapPath(_basePath);
        private static FileSystemWatcher _fsw;

        public static void Initialize(bool reInit)
        {
            _fsw = null;

            if (!Settings.Macros.Enabled) { return; }

            if (Settings.Macros.RunOnStartup)
            {
                Run("System", Monitoring.LogEntry.EntryReason.RunOnStartup, false);
            }
            if (Settings.Macros.MonitorFiles)
            {
                _fsw = new FileSystemWatcher(_baseDir + "\\", "*.cshtml");
                _fsw.IncludeSubdirectories = true;
                _fsw.EnableRaisingEvents = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fsw.Changed += _fsw_Changed;
                _fsw.Deleted += _fsw_Deleted;
                _fsw.Renamed += _fsw_Renamed;
            }
        }

        public static void Run(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            CreateAndUpdate(initiator, reason, test);
            Delete(initiator, reason, test);
        }

        static void _fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                Monitoring.LogEntry logEntry = null;
                if (Path.GetExtension(e.FullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Macro, Path.GetFileNameWithoutExtension(e.FullPath));
                    Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
                }
                if (Path.GetExtension(e.OldFullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.Macro, Path.GetFileNameWithoutExtension(e.OldFullPath));
                    Worker.Queue(e.OldFullPath, new Worker.ProcessFileDelegate(DeleteMacro), logEntry, false);
                }
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        static void _fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Macro, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(DeleteMacro), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void _fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Macro, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void CreateAndUpdate(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            if (!Directory.Exists(_baseDir)) { throw new Exception(_baseDir + " does not exist"); }

            //Get all the partial macros from its folder
            var files = System.IO.Directory.GetFiles(_baseDir, "*.cshtml", SearchOption.AllDirectories);

            //Process each file and check if a macro exists for it. If not: create one
            //Only do so if it changed from the one in the temp folder
            foreach (var file in files)
            {
                var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Macro, Path.GetFileNameWithoutExtension(file));
                Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), logEntry, test);
            }
        }

        private static void Delete(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            var ms = ApplicationContext.Current.Services.MacroService;
            var macros = ms.GetAll();
            foreach (var macro in macros)
            {
                var alias = macro.Alias;
                var scriptingFile = macro.ScriptPath;
                if (!string.IsNullOrEmpty(scriptingFile) && scriptingFile.ToLower().StartsWith("~/views") && scriptingFile.EndsWith(".cshtml"))
                {
                    var file = HostingEnvironment.MapPath(scriptingFile);
                    if (!System.IO.File.Exists(file))
                    {
                        var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.Macro, Path.GetFileNameWithoutExtension(file));
                        Worker.Queue(file, new Worker.ProcessFileDelegate(DeleteMacro), logEntry, test);
                    }
                }
            }
        }

        private static IMacro ProcessFile(string file, Monitoring.LogEntry logEntry, bool test)
        {
            IMacro macro = null;
            try
            {
                //Get the basics
                var scriptingFile = file.Replace(_baseDir, string.Empty).Replace("\\", "/");
                var alias = scriptingFile.TrimStart("/").Replace(".cshtml", string.Empty).Replace("/", "");
                if (alias.Contains(' ')) { return null; }
                string name = null;
                var cachePerPage = Settings.Macros.CachePerPage;
                var cacheTime = Settings.Macros.CacheTime;
                var cachePerMember = Settings.Macros.CachePerMember;
                var useInEditor = Settings.Macros.UseInEditor;
                var renderInEditor = Settings.Macros.RenderInEditor;
                var autoCreate = Settings.Macros.AutoCreate;
                var autoUpdate = Settings.Macros.AutoUpdate;
                var props = new ArrayList();

                //Open the file and get the settings (if they exist)
                //We have to copy it to the temp loc to read it there. If we read it straight at the source it will throw an exception (file is in use)
                var tempFile = Helper.GetTempFile(file);
                var lines = System.IO.File.ReadAllLines(tempFile);
                if (lines.Any())
                {
                    if (lines[0].Trim().ToLower() == "@*sitebuilder")
                    {
                        var i = 1;
                        while (lines.Length > i && lines[i].Trim() != "*@")
                        {
                            var line = lines[i].Trim();
                            var lowered = line.ToLower();
                            if (lowered.StartsWith("alias"))
                            { alias = line.Split('=')[1]; }
                            if (lowered.StartsWith("name"))
                            { name = line.Split('=')[1]; }
                            if (lowered.StartsWith("cachetime"))
                            { cacheTime = int.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("cacheperpage"))
                            { cachePerPage = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("cachepermember"))
                            { cachePerMember = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("useineditor"))
                            { useInEditor = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("renderineditor"))
                            { renderInEditor = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("autocreate"))
                            { autoCreate = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("autoupdate"))
                            { autoUpdate = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("property"))
                            {
                                var propVals = line.Split('=')[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                if (propVals.Length > 0)
                                {
                                    props.Add(new { Alias = propVals[0].Trim(), Type = propVals.Length > 1 ? propVals[1].Trim() : "Umbraco.Textbox", Name = propVals.Length > 2 ? propVals[2].Trim() : propVals[0].Trim() });
                                }
                            }
                            i++;
                        }
                    }
                }

                //Process all the stuff
                var ms = ApplicationContext.Current.Services.MacroService;
                macro = ms.GetByAlias(alias);
                var isNew = false;
                var isDirty = false;
                if (macro == null && autoCreate)
                {
                    isNew = true;
                    isDirty = true;
                    macro = new Umbraco.Core.Models.Macro(alias, name);
                    ms.Save(macro);
                    macro = ms.GetByAlias(alias);
                }
                if ((isNew && autoCreate) || (macro != null && autoUpdate))
                {
                    var val = "~" + _basePath + scriptingFile;
                    macro.SetIfDifferent(() => macro.ScriptPath, val, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.Alias, alias, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.Name, name ?? alias, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.CacheByPage, cachePerPage, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.CacheDuration, cacheTime, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.CacheByMember, cachePerMember, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.UseInEditor, useInEditor, ref isDirty, test);
                    macro.SetIfDifferent(() => macro.DontRender, !renderInEditor, ref isDirty, test);
                    foreach (dynamic item in props)
                    {
                        Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.MacroParameter, item.Alias);
                        try
                        {
                            var propIsDirty = false;
                            var propIsNew = false;
                            var prop = macro.Properties.FirstOrDefault(p => p.Alias == item.Alias);
                            if (prop == null)
                            {
                                try
                                {
                                    prop = new Umbraco.Core.Models.MacroProperty(item.Alias, item.Name, 0, item.Type); //MacroProperty.MakeNew(macro, false, item.Alias, item.Name, item.Type);
                                    macro.Properties.Add(prop);
                                    propIsDirty = true;
                                    propIsNew = true;
                                }
                                catch { }
                            }
                            else
                            {
                                prop.SetIfDifferent(() => prop.Name, (string)item.Name, ref propIsDirty, test);
                                prop.SetIfDifferent(() => prop.EditorAlias, (string)item.Type, ref propIsDirty, test);
                            }
                            if (prop != null)
                            {
                                if (propIsDirty)
                                {
                                    isDirty = true;
                                    subLog.Result = (propIsNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.Updated);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            subLog.Result = Monitoring.LogEntry.EntryResult.Error;
                            subLog.Exception = ex;
                        }
                    }

                    foreach (var propAlias in macro.Properties.Where(mp => !props.ToArray().Any(p => ((dynamic)p).Alias == mp.Alias)).Select(p => p.Alias).ToList())
                    {
                        var subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.MacroParameter, propAlias);
                        try
                        {
                            if (Settings.Macros.AutoDelete)
                            {
                                macro.Properties.Remove(propAlias);
                                isDirty = true;
                                //prop.Delete();
                                subLog.Result = Monitoring.LogEntry.EntryResult.Deleted;
                            }
                            else
                            {
                                subLog.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                            }
                        }
                        catch (Exception ex)
                        {
                            subLog.Result = Monitoring.LogEntry.EntryResult.Error;
                            subLog.Exception = ex;
                        }
                    }
                    if (isDirty)
                    {
                        ms.Save(macro);
                        //macro.Save();
                        logEntry.Result = (isNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.Updated);
                    }
                }
            }
            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }
            Monitoring.Log(logEntry);
            return macro;
        }

        //TODO: Create force delete logic
        //public static void ForceDeleteMacro(string alias, Monitoring.LogEntry logEntry)
        //{
        //    var macro = Macro.GetByAlias(alias);
        //    if (macro != null)
        //    {
        //        macro.Delete();
        //        logEntry.Result = Monitoring.LogEntry.EntryResult.Deleted;
        //    }
        //}

        //public static void ForceDeleteMacroParameter(string macroAlias, string parameterAlias, Monitoring.LogEntry logEntry)
        //{
        //    var macro = Macro.GetByAlias(macroAlias);
        //    if (macro != null)
        //    {
        //        var parm = macro.Properties.FirstOrDefault(p => p.Alias.ToLower() == parameterAlias.ToLower());
        //        if (parm != null)
        //        {
        //            parm.Delete();
        //            logEntry.Result = Monitoring.LogEntry.EntryResult.Deleted;
        //        }
        //    }
        //}

        private static object DeleteMacro(string file, Monitoring.LogEntry logEntry, bool test)
        {
            try
            {
                var scriptingFile = "~" + _basePath + file.Replace(_baseDir, string.Empty).Replace("\\", "/");
                var ms = ApplicationContext.Current.Services.MacroService;
                var macros = ms.GetAll();
                foreach (var macro in macros)
                {
                    if ((macro.ScriptPath ?? string.Empty).ToLower() == scriptingFile.ToLower())
                    {
                        if (Settings.Macros.AutoDelete)
                        {
                            if (test)
                            {
                                logEntry.Result = Monitoring.LogEntry.EntryResult.WouldBeDeleted;
                            }
                            else
                            {
                                ms.Delete(macro);
                                //macro.Delete();
                                logEntry.Result = Monitoring.LogEntry.EntryResult.Deleted;
                            }
                        }
                        else
                        {
                            logEntry.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
                logEntry.Exception = ex;
            }
            Monitoring.Log(logEntry);
            return null;
        }
    }
}