﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class Extensions
    {
        public static string GetValue(this XElement e, string name)
        {
            var attribute = e.Attribute(name);
            if (attribute != null)
            {
                var value = attribute.Value;
                if (!string.IsNullOrEmpty(value))
                {
                    return value;
                }
            }
            return null;
        }

        public static void SetIfDifferent<T>(this Object target, Expression<Func<T>> property, T val, ref bool isDirty, bool test)
        {
            target.SetIfDifferent(property, x => x.Equals(val), val, ref isDirty, test);
        }

        public static void SetIfDifferent<T>(this Object target, Expression<Func<T>> property, Func<dynamic, bool> pred, T val, ref bool isDirty, bool test)
        {
            Expression exp = property.Body;
            MemberExpression mexp = exp as MemberExpression;
            PropertyInfo fieldInfo = mexp.Member as PropertyInfo;
            var cur = (T)fieldInfo.GetValue(target, null);

            if ((cur == null && val != null) || (cur != null && val == null) || !pred(cur))
            {
                isDirty = isDirty | true;
                if (!test)
                {
                    fieldInfo.SetValue(target, val, null);
                }
            }
        }

        public static string ToFileName(this string text)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in text)
            {
                if (char.IsLetterOrDigit(c))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string ToCamelCase(this string text)
        {
            var result = text.ToFileName();
            return char.ToLowerInvariant(result[0]) + result.Substring(1);
        }
    }
}