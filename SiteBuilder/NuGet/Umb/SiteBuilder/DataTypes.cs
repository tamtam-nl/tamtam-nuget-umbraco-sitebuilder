﻿using System.IO;
using System.Web.Hosting;
using Umbraco.Core;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using Umbraco.Core.Models;
using System;
using System.Xml.Serialization;
using TamTam.NuGet.Umb.SiteBuilder.DataModels;
using System.Xml;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class DataTypes
    {
        private static FileSystemWatcher _fsw;
        private static string _basePath = "/SiteBuilder/DataTypes";
        private static string _baseDir = HostingEnvironment.MapPath(_basePath);

        public static void Initialize(bool reInit = false)
        {
            _fsw = null;

            if (!Settings.DataTypes.Enabled) { return; }

            if (Settings.DataTypes.RunOnStartup)
            {
                Run("System", Monitoring.LogEntry.EntryReason.RunOnStartup, false);
            }
            if (Settings.DataTypes.MonitorFiles)
            {
                _fsw = new FileSystemWatcher(_baseDir + "\\", "*.xml");
                _fsw.IncludeSubdirectories = true;
                _fsw.EnableRaisingEvents = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fsw.Changed += _fsw_Changed;
                _fsw.Deleted += _fsw_Deleted;
                _fsw.Renamed += _fsw_Renamed;
            }
        }

        public static void Run(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            CreateAndUpdate(initiator, reason, test);
            //Delete(initiator, reason, test);
        }

        private static void _fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DataType, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        static void _fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                Monitoring.LogEntry logEntry = null;
                if (Path.GetExtension(e.FullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DataType, Path.GetFileNameWithoutExtension(e.FullPath));
                    Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
                }
                if (Path.GetExtension(e.OldFullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.DataType, Path.GetFileNameWithoutExtension(e.OldFullPath));
                    //Worker.Queue(e.OldFullPath, new Worker.ProcessFileDelegate(DeleteDocType), logEntry, false);
                }
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        static void _fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DataType, Path.GetFileNameWithoutExtension(e.FullPath));
                //Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(DeleteDocType), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        public static void CreateAndUpdate(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            if (!Directory.Exists(_baseDir)) { return; }

            //Loop through all the files
            var files = System.IO.Directory.GetFiles(_baseDir, "*.xml", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DataType, Path.GetFileNameWithoutExtension(file));
                Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), logEntry, test);
            }
        }

        private static DataTypeDefinition ProcessFile(string file, Monitoring.LogEntry logEntry, bool test)
        {
            DataTypeDefinition dataTypeDefinition = null;
            try
            {
                //Get the basics
                var alias = Path.GetFileNameWithoutExtension(file);
                if (alias.Contains(" ")) { return null; }

                //Open the file and get the settings (if they exist)
                //We have to copy it to the temp loc to read it there. If we read it straight at the source it will throw an exception (file is in use)
                var tempFile = Helper.GetTempFile(file);
                var xDoc = XDocument.Load(tempFile);
                if (xDoc == null) { return null; }

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(DataType));
                DataType dataType;

                using (var reader = xDoc.Root.CreateReader())
                {
                    dataType = (DataType)xmlSerializer.Deserialize(reader);
                }

                if (dataType == null) { return null; }

                var dts = ApplicationContext.Current.Services.DataTypeService;

                // check if datatype already exists
                bool isNew = dataType.Definition == null || dts.GetDataTypeDefinitionById(new Guid(dataType.Definition)) == null;

                if (isNew)
                {
                    DataTypeDefinition newDataType = new DataTypeDefinition(-1, dataType.Id);

                    newDataType.Name = dataType.Name;
                    newDataType.DatabaseType = (DataTypeDatabaseType)Enum.Parse(typeof(DataTypeDatabaseType), dataType.DatabaseType);

                    // create new datatype
                    CheckPreValuesAndSaveDataType(dts, dataType, newDataType);

                    logEntry.Result = Monitoring.LogEntry.EntryResult.Created;

                    // update Guid in XML file
                    var savedGuid = dts.GetDataTypeDefinitionByName(newDataType.Name).Key;

                    xDoc.Descendants("DataType").FirstOrDefault().SetAttributeValue("Definition", savedGuid.ToString());
                    xDoc.Save(file);
                }
                else
                {
                    // update datatype
                    var savedDefinition = dts.GetDataTypeDefinitionById(new Guid(dataType.Definition));
                    var prevalues = dts.GetPreValuesCollectionByDataTypeId(savedDefinition.Id);

                    savedDefinition.Name = dataType.Name;
                    savedDefinition.DatabaseType = (DataTypeDatabaseType)Enum.Parse(typeof(DataTypeDatabaseType), dataType.DatabaseType);

                    CheckPreValuesAndSaveDataType(dts, dataType, savedDefinition);

                    logEntry.Result = Monitoring.LogEntry.EntryResult.Updated;
                }
            }

            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }
            Monitoring.Log(logEntry);
            return dataTypeDefinition;
        }

        private static void CheckPreValuesAndSaveDataType(Umbraco.Core.Services.IDataTypeService dts, DataType dataType, IDataTypeDefinition definitionToSave)
        {
            if (dataType.PreValues != null && dataType.PreValues.Count() > 0)
            {
                Dictionary<string, PreValue> preValuesDictionary = new Dictionary<string, PreValue>();

                foreach (var preValue in dataType.PreValues.Where(pv => !string.IsNullOrEmpty(pv.Alias)))
                {
                    var value = preValue.Value != null ? preValue.Value : preValue.Value1;

                    preValuesDictionary.Add(preValue.Alias, new PreValue(value));
                }

                // save prevalues && datatype
                dts.SaveDataTypeAndPreValues(definitionToSave, preValuesDictionary);
            }
            else
            {
                // only save datatype
                dts.Save(definitionToSave);
            }
        }

        public static void Export(string initiator, Monitoring.LogEntry.EntryReason reason)
        {
            //Get all documenttypes
            var dts = ApplicationContext.Current.Services.DataTypeService;
            var dataTypes = dts.GetAllDataTypeDefinitions();

            //Process them all
            foreach (var dt in dataTypes)
            {
                var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.Export, Monitoring.LogEntry.EntryEntity.DataType, dt.Name);
                Worker.Queue(dt.Name, new Worker.ProcessFileDelegate(ExportDataType), logEntry, false);
            }
        }

        private static string ExportDataType(string name, Monitoring.LogEntry logEntry, bool test)
        {
            string file = null;
            try
            {
                //Get contentType 
                var dts = ApplicationContext.Current.Services.DataTypeService;
                var dataTypes = dts.GetAllDataTypeDefinitions();
                var dt = dts.GetDataTypeDefinitionByName(name);
                var dtv = dts.GetPreValuesCollectionByDataTypeId(dt.Id);

                ////Get all files (if fold exists)
                var dir = _baseDir.Replace("DataTypes", "Export\\DataTypes");

                if (dt != null)
                {
                    Directory.CreateDirectory(dir);
                    file = Path.Combine(dir, name.ToFileName()) + ".xml";


                    //Create a new xDoc
                    var xDoc = new XDocument(
                        new XElement("DataType",
                            new XAttribute("Name", name),
                            new XAttribute("Id", dt.PropertyEditorAlias),
                            new XAttribute("Definition", dt.Key.ToString()),
                            new XAttribute("DatabaseType", dt.DatabaseType.ToString()),

                            new XElement("PreValues", dtv.PreValuesAsDictionary.Select(
                                pv => new XElement("PreValue",
                                        new XAttribute("Id", pv.Value.Id),
                                        new XAttribute("Alias", pv.Key),
                                        !string.IsNullOrEmpty(pv.Value.Value) && IsJson(pv.Value.Value) ? new XCData(pv.Value.Value) : null,
                                        !string.IsNullOrEmpty(pv.Value.Value) && !IsJson(pv.Value.Value) ? pv.Value.Value : null)
                                ))
                            )
                        );

                    //Save it to file
                    if (test)
                    {
                        logEntry.Result = Monitoring.LogEntry.EntryResult.WouldBeExported;
                    }
                    else
                    {
                        xDoc.Save(file);

                        logEntry.Result = Monitoring.LogEntry.EntryResult.Exported;
                    }
                }
            }
            catch (Exception ex)
            {
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
                logEntry.Exception = ex;
            }
            Monitoring.Log(logEntry);
            return file;
        }

        private static bool IsJson(string input)
        {
            input = input.Trim();
            return input.StartsWith("{") && input.EndsWith("}")
                   || input.StartsWith("[") && input.EndsWith("]");
        }
    }
}