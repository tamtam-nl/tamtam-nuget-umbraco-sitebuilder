﻿using System;
using System.Web;
using Umbraco.Core.Services;
using Umbraco.Core.Security;

namespace TamTam.NuGet.Umb.SiteBuilder.HttpHandlers
{
    public class Manager : IHttpHandler
    {

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                if (InternalCurrentUser() == null)
                {
                    context.Response.StatusCode = 403;
                    return;
                }

                var action = context.Request.QueryString["action"] ?? string.Empty;
                switch (action)
                {
                    case "import":
                        var test = bool.Parse(context.Request.QueryString["test"] ?? "false");
                        Import(test);
                        break;
                    case "clearLogs": Monitoring.ClearLogs(); break;
                    case "readLogs":
                        var fromIdx = int.Parse(context.Request.QueryString["from"] ?? "0");
                        context.Response.Write(Monitoring.ReadLogFile(fromIdx)); break;
                    case "export": Export(); break;
                    case "lastLogIdx": context.Response.Write(Monitoring.LastLogIdx()); break;
                        //TODO: Create force delete logic
                        //case "delete":
                        //    var entity = (string)dynData.Entity;
                        //    var name = (string)dynData.Name;
                        //    Delete(entity, name);
                        //    break;
                }
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void Import(bool test)
        {
            var user = InternalCurrentUser().Name;
            if (Settings.DataTypes.Enabled)
            {
                DataTypes.Run(user, Monitoring.LogEntry.EntryReason.StartedByUser, test);
            }
            if (Settings.Macros.Enabled)
            {
                Macros.Run(user, Monitoring.LogEntry.EntryReason.StartedByUser, test);
            }
            if (Settings.Templates.Enabled)
            {
                Templates.Run(user, Monitoring.LogEntry.EntryReason.StartedByUser, test);
            }
            if (Settings.DocTypes.Enabled)
            {
                DocTypes.Run(user, Monitoring.LogEntry.EntryReason.StartedByUser, test);
            }
        }

        private static void Export()
        {
            var user = InternalCurrentUser().Name;
            DocTypes.Export(user, Monitoring.LogEntry.EntryReason.StartedByUser);
            DataTypes.Export(user, Monitoring.LogEntry.EntryReason.StartedByUser);
        }

        private static Umbraco.Core.Models.Membership.IUser InternalCurrentUser()
        {
            var userService = Umbraco.Core.ApplicationContext.Current.Services.UserService;

            // Snippet from: http://issues.umbraco.org/issue/U4-6342#comment=67-19466
            var httpCtxWrapper = new System.Web.HttpContextWrapper(System.Web.HttpContext.Current);
            var umbTicket = httpCtxWrapper.GetUmbracoAuthTicket();

            if (umbTicket == null || string.IsNullOrEmpty(umbTicket.Name) || umbTicket.Expired) return null;

            var user = userService.GetByUsername(umbTicket.Name);
            return user;
        }

        //private void WriteStatus(string text, bool lineBreak = false)
        //{
        //    HttpContext.Current.Response.Write("<p style='font-family:courier;font-size:9'>" + text + "</p>");
        //    HttpContext.Current.Response.Flush();
        //}
    }
}