﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using log4net.Core;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class Monitoring
    {
        public delegate void LogHandler(LogEntryEventArgs e);
        public static event LogHandler NewLogEntry;
        public static HttpContext Context;

        private static string _logFile = HostingEnvironment.MapPath("/app_data/sitebuilder/log.txt");
        private static object _sysObject = new object();
        private static int _entryId = 0;

        static Monitoring()
        {
            if (File.Exists(_logFile))
            {
                lock (_sysObject)
                {
                    var lastLine = string.Empty;
                    using (FileStream fs = new FileStream(_logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var sr = new System.IO.StreamReader(fs);
                        while (sr.Peek() != -1)
                        {
                            lastLine = sr.ReadLine();
                        }
                    }

                    var elms = lastLine.Split(',');
                    if (elms.Length > 0)
                    {
                        int.TryParse(elms[0], out _entryId);
                    }
                }
            }
        }

        public static int LastLogIdx()
        {
            return _entryId;
        }

        public static void ClearLogs()
        {
            if (File.Exists(_logFile))
            {
                File.Delete(_logFile);
                _entryId = 0;
            }
        }

        public static string ReadLogFile(int fromIdx)
        {
            if (File.Exists(_logFile))
            {
                lock (_sysObject)
                {
                    var data = new StringBuilder();
                    using (FileStream fs = new FileStream(_logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var sr = new System.IO.StreamReader(fs);
                        while (sr.Peek() != -1)
                        {
                            var line = sr.ReadLine();
                            if (!string.IsNullOrEmpty(line) && int.Parse(line.Split(',')[0]) > fromIdx)
                            {
                                data.AppendLine(line);
                            }
                        }
                    }
                    return data.ToString();
                }
            }
            return "no-data";
        }

        public static void Log(LogEntry entry, int level = 1)
        {
            lock (_sysObject)
            {
                _entryId++;
                var text = new StringBuilder();
                text.Append(_entryId.ToString() + ",");
                text.Append(entry.Time.ToString("yyyy-MM-dd HH:mm:ss") + ",");
                text.Append(level.ToString() + ",");
                text.Append(entry.Initiator + ",");
                text.Append(entry.Reason.ToString() + ",");
                text.Append(entry.Action.ToString() + ",");
                text.Append(entry.Entity + ",");
                text.Append(entry.EntityName + ",");
                text.Append(entry.Result.ToString() + ',');
                text.Append((entry.SubLogItems.Count > 0).ToString() + ",");
                text.Append(entry.Exception != null ? entry.Exception.ToString().Replace("\r\n", "|").Replace(',', ' ') : string.Empty);
                var data = text.ToString();
                LogToFile(_logFile, data);
                OnNewLogEntry(data);

                foreach (var subEntry in entry.SubLogItems)
                {
                    Log(subEntry, level + 1);
                }
            }
        }

        public static void LogException(Exception ex)
        {
            ILog logger = LogManager.GetLogger(string.Empty);
            logger.Error(ex);
        }

        private static void LogException(string file, Exception ex)
        {
            if (ex != null)
            {
                LogToFile(file, ex.Message);
                if (ex.StackTrace != null)
                {
                    System.IO.File.AppendAllLines(file, (ex.StackTrace.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).AsEnumerable()));
                    //using (FileStream fs = new FileStream(file, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                    //{
                    //    var sw = new System.IO.StreamWriter(fs);
                    //foreach (var line in (ex.StackTrace.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).AsEnumerable()))
                    //{
                    //sw.WriteLineAsync(line);
                    //}
                    //}
                }
                if (ex.InnerException != null)
                {
                    LogException(file, ex.InnerException);
                }
            }
        }

        private static void LogToFile(string file, string text)
        {
            try
            {
                lock (_sysObject)
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(file));
                    //using (FileStream fs = new FileStream(file, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                    //{
                    //    var sw = new System.IO.StreamWriter(fs);
                    //foreach (var line in (new string[] { text }).AsEnumerable())
                    //{
                    //sw.WriteLineAsync(line);
                    //}
                    //}
                    System.IO.File.AppendAllLines(file, (new string[] { text }).AsEnumerable());
                }
            }
            catch { }
        }

        private static void OnNewLogEntry(string data)
        {
            // Make sure someone is listening to event
            if (NewLogEntry == null) return;

            LogEntryEventArgs args = new LogEntryEventArgs(data);
            NewLogEntry(args);
        }

        public class LogEntryEventArgs : EventArgs
        {
            public string Data { get; private set; }
            public LogEntryEventArgs(string data)
            {
                this.Data = data;
            }
        }

        public class LogEntry
        {
            public enum EntryResult
            {
                NothingToDo = 0,
                WouldBeExported = 1,
                Exported = 2,
                HasDifferences = 3,
                NotInSolution = 4,
                WouldBeUpdated = 5,
                WouldBeCreated = 6,
                WouldBeDeleted = 7,
                Updated = 8,
                Created = 9,
                Deleted = 10,
                Error = 11,
            }

            public enum EntryAction
            {
                CheckForChanges = 1,
                CheckForDeletion = 2,
                Export = 3,
                LinkToTemplate = 4,
                ForceDelete = 5,
                AllowUnder = 6
            }

            public enum EntryReason
            {
                RunOnStartup = 1,
                FileMonitor = 2,
                StartedByUser = 3
            }

            public enum EntryEntity
            {
                Unknown = 0,
                Macro = 1,
                MacroParameter = 2,
                Template = 3,
                DocumentType = 4,
                DocumentTypeTab = 5,
                DocumentTypeProperty = 6,
                DocumentTypeComposition = 7,
                DataType = 8
            }


            public LogEntry Parent { get; private set; }
            public DateTime Time { get; private set; }
            private EntryResult _result = EntryResult.NothingToDo;
            public EntryResult Result
            {
                get
                {
                    return _result;
                }
                set
                {
                    _result = value;
                    if (this.Parent != null && this.Result != EntryResult.NothingToDo)
                    {
                        if (this.Result == EntryResult.Error)
                        {
                            this.Parent.Result = _result;
                        }
                        else if (this.Parent.Result <= this.Result)
                        {
                            switch (this.Result)
                            {
                                case EntryResult.HasDifferences: this.Parent.Result = EntryResult.HasDifferences; break;
                                case EntryResult.NotInSolution: this.Parent.Result = EntryResult.HasDifferences; break;
                                case EntryResult.WouldBeUpdated: this.Parent.Result = EntryResult.WouldBeUpdated; break;
                                case EntryResult.WouldBeCreated: this.Parent.Result = EntryResult.WouldBeUpdated; break;
                                case EntryResult.WouldBeDeleted: this.Parent.Result = EntryResult.WouldBeUpdated; break;
                                case EntryResult.Updated: this.Parent.Result = EntryResult.Updated; break;
                                case EntryResult.Created: this.Parent.Result = EntryResult.Updated; break;
                                case EntryResult.Deleted: this.Parent.Result = EntryResult.Updated; break;
                            }
                        }
                    }
                }
            }
            public string Initiator { get; private set; }
            public EntryReason Reason { get; private set; }
            public EntryEntity Entity { get; private set; }
            public EntryAction Action { get; private set; }
            public Exception Exception { get; set; }
            public string EntityName { get; private set; }
            private List<LogEntry> _subLogItems = new List<LogEntry>();

            public LogEntry(string initiator, EntryReason reason, EntryAction action, EntryEntity entity, string entityName)
            {
                this.Time = DateTime.Now;
                this.Initiator = initiator;
                this.Reason = reason;
                this.Action = action;
                this.Entity = entity;
                this.EntityName = entityName;
                this.Result = EntryResult.NothingToDo;
            }

            public ReadOnlyCollection<LogEntry> SubLogItems
            {
                get
                {
                    return _subLogItems.AsReadOnly();
                }
            }

            public LogEntry NewSubEntry(EntryAction action, EntryEntity entity, string entityName)
            {
                var logItem = new LogEntry(this.Initiator, this.Reason, action, entity, entityName);
                logItem.Parent = this;
                _subLogItems.Add(logItem);
                return logItem;
            }
        }
    }


}