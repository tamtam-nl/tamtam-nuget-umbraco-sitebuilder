﻿using System.IO;
using System.Web.Hosting;
using Umbraco.Core;
using System.Linq;
using umbraco.cms.businesslogic.template;
using System;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class Templates
    {
        private static FileSystemWatcher _fsw;
        private static string _basePath = "/Views";
        private static string _baseDir = HostingEnvironment.MapPath(_basePath);

        public static void Initialize(bool reInit)
        {
            _fsw = null;

            if (!Settings.Templates.Enabled) { return; }
            if (Settings.Templates.RunOnStartup)
            {
                Run("System", Monitoring.LogEntry.EntryReason.RunOnStartup, false);
            }

            if (Settings.Templates.MonitorFiles)
            {
                _fsw = new FileSystemWatcher(_baseDir + "\\", "*.cshtml");
                _fsw.IncludeSubdirectories = false;
                _fsw.EnableRaisingEvents = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fsw.Changed += _fsw_Changed;
                _fsw.Deleted += _fsw_Deleted;
                _fsw.Renamed += _fsw_Renamed;
            }
        }

        static void _fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                Monitoring.LogEntry logEntry = null;
                if (Path.GetExtension(e.FullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Template, Path.GetFileNameWithoutExtension(e.FullPath));
                    Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
                }
                if (Path.GetExtension(e.OldFullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.Template, Path.GetFileNameWithoutExtension(e.OldFullPath));
                    Worker.Queue(e.OldFullPath, new Worker.ProcessFileDelegate(DeleteTemplate), logEntry, false);
                }
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        static void _fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Template, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(DeleteTemplate), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void _fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Template, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        public static void Run(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            CreateAndUpdate(initiator, reason, test);
            Delete(initiator, reason, test);
        }

        public static void CreateAndUpdate(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            if (!Directory.Exists(_baseDir)) { return; }

            //Get all the partial macros from its folder
            var files = System.IO.Directory.GetFiles(_baseDir, "*.cshtml", SearchOption.TopDirectoryOnly);

            //Process each file and check if a template exists for it. If not: create one
            if (files.Any())
            {
                foreach (var file in files)
                {
                    var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.Template, Path.GetFileNameWithoutExtension(file));
                    Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), logEntry, test);
                }
            }
        }

        public static void Delete(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            //Get all Templates and see if their files still exist
            var templates = Template.GetAllAsList();
            foreach (var template in templates)
            {
                var templateFile = template.TemplateFilePath;
                if (!string.IsNullOrEmpty(templateFile) && templateFile.EndsWith(".cshtml"))
                {
                    if (!System.IO.File.Exists(templateFile))
                    {
                        var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.Template, Path.GetFileNameWithoutExtension(templateFile));
                        Worker.Queue(templateFile, new Worker.ProcessFileDelegate(DeleteTemplate), logEntry, test);
                    }
                }
            }
        }

        public static Template ProcessFile(string file, Monitoring.LogEntry logEntry, bool test)
        {
            Template template = null;
            try
            {
                var rendering = Helper.ReadConfigSetting("config\\UmbracoSettings.config", "settings/templates/defaultRenderingEngine");
                if (rendering.ToLower() != "mvc") { throw new Exception("Render engine not set to Mvc"); }

                var templateFile = Path.GetFileName(file);
                var alias = Path.GetFileNameWithoutExtension(templateFile);
                if (alias.Contains(' ')) { return null; }
                string name = null;
                var autoCreate = Settings.Templates.AutoCreate;
                var autoUpdate = Settings.Templates.AutoUpdate;
                string parent = null;

                //Open the file and get the settings (if they exist)
                //We have to copy it to the temp loc to read it there. If we read it straight at the source it will restart the apppool.
                var temp = Helper.GetTempFile(file);
                var lines = System.IO.File.ReadAllLines(temp);
                if (lines.Any())
                {
                    var i = 0;
                    if (lines[i].ToLower().Trim() == "@*sitebuilder")
                    {
                        i++;
                        while (lines.Length > i && lines[i].Trim() != "*@")
                        {
                            var line = lines[i].Trim();
                            var lowered = line.ToLower();

                            if (lowered.StartsWith("name"))
                            { name = line.Split('=')[1]; }
                            if (lowered.StartsWith("autocreate"))
                            { autoCreate = bool.Parse(line.Split('=')[1]); }
                            if (lowered.StartsWith("autoupdate"))
                            { autoUpdate = bool.Parse(line.Split('=')[1]); }
                            i++;
                        }
                    }
                    while (lines.Length > i && parent == null)
                    {
                        var line = lines[i].Trim().Replace(" ", "");
                        var lowered = line.ToLower();
                        if (lowered.StartsWith("layout=\""))
                        {
                            parent = lowered.TrimEnd(';').Replace("\"", "").Replace("layout=", "").TrimEnd(".cshtml");
                        }
                        i++;
                    }
                }

                //Process all the stuff 
                template = Template.GetByAlias(alias);
                var isNew = false;
                var isDirty = false;
                Template master = null;
                if (!string.IsNullOrEmpty(parent))
                {
                    master = Template.GetByAlias(parent);
                }
                if (template == null && autoCreate)
                {
                    isDirty = true;
                    isNew = true;
                    template = Template.MakeNew(alias, umbraco.BusinessLogic.User.GetUser(0));
                }
                if ((isNew && autoCreate) || (template != null && autoUpdate))
                {
                    template.SetIfDifferent(() => template.Text, name ?? alias, ref isDirty, test);
                    if (master != null)
                    {
                        template.SetIfDifferent(() => template.MasterTemplate, master.Id, ref isDirty, test);
                    }
                    else
                    {
                        template.SetIfDifferent(() => template.MasterTemplate, 0, ref isDirty, test);
                    }
                    if (Settings.Templates.AutoLinkToDocType)
                    {
                        var subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.LinkToTemplate, Monitoring.LogEntry.EntryEntity.DocumentType, template.Alias);
                        LinkToDocType(template, subLog);
                    }
                    if (isDirty)
                    {
                        if (test)
                        {
                            logEntry.Result = (isNew ? Monitoring.LogEntry.EntryResult.WouldBeCreated : Monitoring.LogEntry.EntryResult.WouldBeCreated);
                        }
                        else
                        {
                            template.Save();
                            logEntry.Result = (isNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.Updated);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }
            Monitoring.Log(logEntry);
            return template;
        }

        private static void LinkToDocType(Template template, Monitoring.LogEntry logEntry)
        {
            try
            {
                var alias = template.Alias;
                var fs = ApplicationContext.Current.Services.FileService;

                var cts = ApplicationContext.Current.Services.ContentTypeService;
                var ct = cts.GetContentType(alias);
                if (ct != null)
                {
                    var allowed = ct.AllowedTemplates;
                    if (!allowed.Any(t => t.Alias == alias))
                    {
                        ct.AllowedTemplates = allowed.Union(fs.GetTemplates(alias));
                        cts.Save(ct);
                        logEntry.Result = Monitoring.LogEntry.EntryResult.Updated;
                    }
                }
            }
            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }

        }

        private static object DeleteTemplate(string file, Monitoring.LogEntry logEntry, bool test)
        {
            try
            {
                var rendering = Helper.ReadConfigSetting("config\\UmbracoSettings.config", "settings/templates/defaultRenderingEngine");
                if (rendering.ToLower() != "mvc") { throw new Exception("Render engine not set to Mvc"); }

                var templateFile = Path.GetFileName(file);
                var alias = Path.GetFileNameWithoutExtension(templateFile);
                if (alias.Contains(' ')) { return null; }
                var template = Template.GetByAlias(alias);
                if (template != null)
                {
                    if (Settings.Templates.AutoDelete)
                    {
                        if (test)
                        {
                            logEntry.Result = Monitoring.LogEntry.EntryResult.WouldBeDeleted;
                        }
                        else
                        {
                            template.delete();
                            logEntry.Result = Monitoring.LogEntry.EntryResult.Deleted;
                        }
                    }
                    else
                    {
                        logEntry.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                    }
                }
            }
            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }
            Monitoring.Log(logEntry);
            return null;
        }
    }
}
