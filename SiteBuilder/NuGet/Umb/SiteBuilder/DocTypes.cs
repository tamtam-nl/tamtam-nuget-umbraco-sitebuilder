﻿using System.IO;
using System.Web.Hosting;
using Umbraco.Core;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using Umbraco.Core.Models;
using System;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class DocTypes
    {
        private static FileSystemWatcher _fsw;
        private static FileSystemWatcher _fswThumbs;
        private static FileSystemWatcher _fswIcons;
        private static string _basePath = "/SiteBuilder/DocTypes";
        private static string _baseDir = HostingEnvironment.MapPath(_basePath);
        private static string _iconsDir = HostingEnvironment.MapPath("/umbraco/images/umbraco");
        private static string _thumbsDir = HostingEnvironment.MapPath("/umbraco/images/thumbnails");

        public static void Initialize(bool reInit = false)
        {
            _fsw = null;
            _fswThumbs = null;
            _fswIcons = null;

            if (!Settings.DocTypes.Enabled) { return; }

            if (Settings.DocTypes.RunOnStartup)
            {
                Run("System", Monitoring.LogEntry.EntryReason.RunOnStartup, false);
            }
            if (Settings.DocTypes.MonitorFiles)
            {
                _fsw = new FileSystemWatcher(_baseDir + "\\", "*.xml");
                _fsw.IncludeSubdirectories = true;
                _fsw.EnableRaisingEvents = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fsw.Changed += _fsw_Changed;
                _fsw.Deleted += _fsw_Deleted;
                _fsw.Renamed += _fsw_Renamed;

                _fswIcons = new FileSystemWatcher(_iconsDir + "\\");
                _fswIcons.IncludeSubdirectories = false;
                _fswIcons.EnableRaisingEvents = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fswIcons.Changed += _fswIconsOrThumbs_ChangedOrDeleted;
                _fswIcons.Deleted += _fswIconsOrThumbs_ChangedOrDeleted;
                _fswIcons.Renamed += _fswIconsOrThumbs_Renamed;

                _fswThumbs = new FileSystemWatcher(_thumbsDir + "\\");
                _fswThumbs.IncludeSubdirectories = false;
                _fswThumbs.EnableRaisingEvents = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.Security;
                _fswThumbs.Changed += _fswIconsOrThumbs_ChangedOrDeleted;
                _fswThumbs.Deleted += _fswIconsOrThumbs_ChangedOrDeleted;
                _fswThumbs.Renamed += _fswIconsOrThumbs_Renamed;
            }
        }

        public static void Run(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            CreateAndUpdate(initiator, reason, test);
            Delete(initiator, reason, test);
        }

        private static void _fswIconsOrThumbs_ChangedOrDeleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                var alias = Path.GetFileNameWithoutExtension(e.FullPath);
                CheckDocTypeForAlias(alias, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void _fswIconsOrThumbs_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                var alias = Path.GetFileNameWithoutExtension(e.FullPath);
                CheckDocTypeForAlias(alias, false);
                alias = Path.GetFileNameWithoutExtension(e.OldFullPath);
                CheckDocTypeForAlias(alias, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        private static void CheckDocTypeForAlias(string alias, bool test)
        {
            var files = Directory.GetFiles(_baseDir, alias + ".xml", SearchOption.AllDirectories);
            if (files != null && files.Length > 0)
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(files[0]));
                Worker.Queue(files[0], new Worker.ProcessFileDelegate(ProcessFile), logEntry, test);
            }
        }

        private static void _fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        static void _fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                Monitoring.LogEntry logEntry = null;
                if (Path.GetExtension(e.FullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(e.FullPath));
                    Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(ProcessFile), logEntry, false);
                }
                if (Path.GetExtension(e.OldFullPath).ToLower() == ".cshtml")
                {
                    logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(e.OldFullPath));
                    Worker.Queue(e.OldFullPath, new Worker.ProcessFileDelegate(DeleteDocType), logEntry, false);
                }
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        static void _fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                var logEntry = new Monitoring.LogEntry("System", Monitoring.LogEntry.EntryReason.FileMonitor, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(e.FullPath));
                Worker.Queue(e.FullPath, new Worker.ProcessFileDelegate(DeleteDocType), logEntry, false);
            }
            catch (Exception ex)
            {
                Monitoring.LogException(ex);
            }
        }

        public static void CreateAndUpdate(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            if (!Directory.Exists(_baseDir)) { return; }

            //Loop through all the files
            var files = System.IO.Directory.GetFiles(_baseDir, "*.xml", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(file));
                Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), logEntry, test);
            }
        }

        private static ContentType ProcessFile(string file, Monitoring.LogEntry logEntry, bool test)
        {
            ContentType ct = null;
            try
            {
                //Get the basics
                var alias = Path.GetFileNameWithoutExtension(file);
                if (alias.Contains(" ")) { return null; }

                //Open the file and get the settings (if they exist)
                //We have to copy it to the temp loc to read it there. If we read it straight at the source it will throw an exception (file is in use)
                var tempFile = Helper.GetTempFile(file);
                var xDoc = XDocument.Load(tempFile);
                if (xDoc == null) { return null; }
                var root = xDoc.Root;

                //Check auto values
                var autoCreate = bool.Parse(root.GetValue("autoCreate") ?? Settings.DocTypes.AutoCreateDocTypes.ToString());
                var autoUpdate = bool.Parse(root.GetValue("autoUpdate") ?? Settings.DocTypes.AutoUpdateDocTypes.ToString());
                if (!autoCreate && !autoUpdate) { return null; }

                //Check if alias matches filename
                if (root.Name.LocalName != alias) { return null; }

                //Check master
                var cts = ApplicationContext.Current.Services.ContentTypeService;
                var masterAlias = root.GetValue("master");
                ContentType master = null;
                if (!string.IsNullOrEmpty(masterAlias))
                {
                    master = cts.GetContentType(masterAlias) as ContentType;
                    //if (master == null)
                    //{
                    var masterFile = System.IO.Directory.GetFiles(_baseDir, masterAlias + ".xml", SearchOption.AllDirectories).FirstOrDefault();
                    if (masterFile == null) { return null; }
                    Monitoring.LogEntry newLogEntry = new Monitoring.LogEntry(logEntry.Initiator, logEntry.Reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(masterFile));
                    var done = Worker.Queue(masterFile, new Worker.ProcessFileDelegate(ProcessFile), newLogEntry, test);
                    if (!done)
                    {
                        newLogEntry = new Monitoring.LogEntry(logEntry.Initiator, logEntry.Reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(file));
                        Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), newLogEntry, test);
                        return null;
                    }
                    //Monitoring.LogEntry newLogEntry = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(masterFile));
                    //master = ProcessFile(masterFile, newLogEntry, test);
                    if (master == null) { return null; }
                    //}
                }

                //Check if allow under doctypes exist
                var allowUnder = (root.GetValue("allowUnder") ?? string.Empty).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                //foreach (var allowUnderAlias in allowUnder)
                //{
                //    ContentType allowUnderCt = null;
                //    allowUnderCt = cts.GetContentType(allowUnderAlias) as ContentType;
                //    if (allowUnderCt == null)
                //    {
                //        var allowUnderFile = System.IO.Directory.GetFiles(_baseDir, allowUnderAlias + ".xml", SearchOption.AllDirectories).FirstOrDefault();
                //        if (allowUnderFile != null)
                //        {
                //            Monitoring.LogEntry newLogEntry = new Monitoring.LogEntry(logEntry.Initiator, logEntry.Reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(allowUnderFile));
                //            var done = Worker.Queue(allowUnderFile, new Worker.ProcessFileDelegate(ProcessFile), newLogEntry, test);
                //            if (!done)
                //            {
                //                newLogEntry = new Monitoring.LogEntry(logEntry.Initiator, logEntry.Reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(file));
                //                Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), newLogEntry, test);
                //                return null;
                //            }
                //        }
                //    }
                //}

                //Create or find Doctype
                ct = cts.GetContentType(alias) as ContentType;
                var isNew = false;
                var isDirty = false;

                //Check for possible deletions
                var compositions = (root.GetValue("compositions") ?? string.Empty).ToLower().Split(',');
                IEnumerable<XElement> tabElms = root.Elements();
                if (ct != null)
                {
                    var allPropElms = new List<XElement>();
                    foreach (var elm in tabElms)
                    {
                        allPropElms.AddRange(elm.Elements());
                    }
                    foreach (var tab in ct.PropertyGroups)
                    {
                        if (!tabElms.Any(te => te.Name.LocalName.ToLower().Replace('_', ' ') == tab.Name.ToLower()))
                        {
                            Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.DocumentTypeTab, tab.Name);
                            subLog.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                        }
                    }
                    foreach (var prop in ct.PropertyTypes.Where(pt => !allPropElms.Any(pe => pe.Name.LocalName.ToLower() == pt.Alias.ToLower())))
                    {
                        Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.DocumentTypeProperty, prop.Alias);
                        subLog.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                    }
                    var removedCompositions = ct.CompositionAliases().Select(ca => ca.ToLower()).Except(compositions);
                    foreach (var compAlias in removedCompositions)
                    {
                        Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.DocumentTypeComposition, compAlias);
                        subLog.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                    }
                }

                if (ct == null && autoCreate)
                {
                    if (master != null)
                    {
                        ct = new ContentType(master, alias);
                    }
                    else
                    {
                        ct = new ContentType(-1);
                        ct.Alias = alias;
                    }
                    isNew = true;
                    isDirty = true;
                }

                //Set base doctype settings
                if ((isNew && autoCreate) || (ct != null && autoUpdate))
                {
                    var fs = ApplicationContext.Current.Services.FileService;
                    var imagesPath = HostingEnvironment.MapPath("/umbraco/images");
                    ct.SetIfDifferent(() => ct.Name, root.GetValue("name") ?? ct.Alias, ref isDirty, test);
                    ct.SetIfDifferent(() => ct.AllowedAsRoot, bool.Parse(root.GetValue("allowAtRoot") ?? "false"), ref isDirty, test);
                    var xmlTemplates = (root.GetValue("templates") ?? alias).ToLower().Split(',');
                    var templates = fs.GetTemplates(xmlTemplates).Where(t => t != null);
                    var changed = (templates.Except(ct.AllowedTemplates).Count() > 0);
                    ct.SetIfDifferent(() => ct.AllowedTemplates, x => !changed, templates, ref isDirty, test);

                    var addedAliases = compositions.Except(ct.CompositionAliases().Select(ca => ca.ToLower()));
                    foreach (var compAlias in addedAliases)
                    {
                        var compCt = cts.GetContentType(compAlias);
                        if (compCt != null)
                        {
                            ct.AddContentType(compCt);
                            isDirty = true;
                        }
                    }
                    ct.SetIfDifferent(() => ct.Description, root.GetValue("description") ?? string.Empty, ref isDirty, test);
                    var icon = root.GetValue("icon");
                    if (icon == null) { icon = Directory.GetFiles(Path.Combine(imagesPath, "umbraco"), ct.Alias + ".*", SearchOption.TopDirectoryOnly).Select(i => Path.GetFileName(i)).FirstOrDefault(); }
                    if (string.IsNullOrEmpty(icon)) { icon = (ct.Alias.ToLower().Contains("folder") ? "folder.gif" : (ct.Alias.ToLower().Contains("data") ? "mediaFile.gif" : "docPic.gif")); }
                    ct.SetIfDifferent(() => ct.Icon, icon, ref isDirty, test);
                    var thumb = root.GetValue("thumb");
                    if (thumb == null) { thumb = Directory.GetFiles(Path.Combine(imagesPath, "thumbnails"), ct.Alias + ".*", SearchOption.TopDirectoryOnly).Select(i => Path.GetFileName(i)).FirstOrDefault(); }
                    if (string.IsNullOrEmpty(thumb)) { thumb = (ct.Alias.ToLower().Contains("folder") ? "folder.png" : (ct.Alias.ToLower().Contains("data") ? "mediaFile.png" : "docWithImage.png")); }
                    ct.SetIfDifferent(() => ct.Thumbnail, thumb, ref isDirty, test);

                    //If new, we better do an intermediate save here so the doctype exists and does not cause trouble allong the way.
                    if (isNew && !test)
                    {
                        cts.Save(ct); ct = cts.GetContentType(alias) as ContentType;
                    }

                    //Check structure
                    var requeue = false;
                    foreach (var item in allowUnder)
                    {
                        Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.AllowUnder, Monitoring.LogEntry.EntryEntity.DocumentType, item);
                        try
                        {
                            IContentType structCt = null;
                            if (item.ToLower() == alias.ToLower())
                            {
                                structCt = ct;
                            }
                            else
                            {
                                structCt = cts.GetContentType(item);
                            }
                            if (structCt != null)
                            {
                                var curSubs = structCt.AllowedContentTypes.ToList();
                                if (!curSubs.Any(t => t.Id.Value == ct.Id))
                                {
                                    var sortOrder = (curSubs.Count != 0 ? curSubs.Max(t => t.SortOrder) + 1 : 1);
                                    curSubs.Add(new ContentTypeSort() { Id = new Lazy<int>(() => ct.Id), SortOrder = sortOrder });
                                    structCt.AllowedContentTypes = curSubs;
                                    if (test)
                                    {
                                        subLog.Result = Monitoring.LogEntry.EntryResult.WouldBeUpdated;
                                    }
                                    else if (item.ToLower() != alias.ToLower())
                                    {
                                        cts.Save(structCt);
                                        subLog.Result = Monitoring.LogEntry.EntryResult.Updated;
                                    }
                                }
                            }
                            else
                            {
                                requeue = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            subLog.Result = Monitoring.LogEntry.EntryResult.Error;
                            subLog.Exception = ex;
                        }
                    }
                    if (requeue)
                    {
                        //requeue it
                        var newLogEntry = new Monitoring.LogEntry(logEntry.Initiator, logEntry.Reason, Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentType, Path.GetFileNameWithoutExtension(file));
                        Worker.Queue(file, new Worker.ProcessFileDelegate(ProcessFile), newLogEntry, test);
                    }


                    //Create tabs and props
                    var sortorder = 0;
                    foreach (var tabElement in tabElms)
                    {
                        var tabAlias = tabElement.Name.LocalName.Replace("_", " ");
                        Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentTypeTab, tabAlias);
                        try
                        {
                            var tabIsDirty = false;
                            var tabIsNew = false;
                            PropertyGroup tab = null;
                            if (tabAlias.ToLower() != "generic")
                            {
                                sortorder = int.Parse(tabElement.GetValue("sort") ?? (sortorder + 1).ToString());
                                tab = ct.PropertyGroups.FirstOrDefault(pg => pg.Name.ToLower() == tabAlias.ToLower());
                                if (tab == null && ct.CompositionPropertyGroups.Any(pg => pg.Name.ToLower() == tabAlias.ToLower()))
                                {
                                    tabIsDirty = true;
                                    ct.AddPropertyGroup(tabAlias);
                                    tab = ct.PropertyGroups.FirstOrDefault(pg => pg.Name.ToLower() == tabAlias.ToLower());
                                }
                                if (tab == null)
                                {
                                    tab = new PropertyGroup();
                                    ct.PropertyGroups.Add(tab);
                                    tabIsNew = true;
                                    tabIsDirty = true;
                                }
                                tab.SetIfDifferent(() => tab.Name, tabAlias, ref tabIsDirty, test);
                                tab.SetIfDifferent(() => tab.SortOrder, sortorder, ref tabIsDirty, test);

                                //after adding tabs it seems wise to do an intermediate save;
                                if (tabIsDirty)
                                {
                                    if (test)
                                    {
                                        subLog.Result = (tabIsNew ? Monitoring.LogEntry.EntryResult.WouldBeCreated : Monitoring.LogEntry.EntryResult.WouldBeUpdated);
                                    }
                                    else
                                    {
                                        cts.Save(ct); ct = cts.GetContentType(alias) as ContentType; tab = ct.PropertyGroups.FirstOrDefault(pg => pg.Name.ToLower() == tabAlias.ToLower());
                                        subLog.Result = (tabIsNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.Updated);
                                    }
                                }
                            }

                            //Process the props
                            var propElements = tabElement.Elements();
                            ProcessProperties(ct, tab, propElements, ref isDirty, subLog, test);
                        }
                        catch (Exception ex)
                        {
                            subLog.Result = Monitoring.LogEntry.EntryResult.Error;
                            subLog.Exception = ex;
                        }
                    }
                }


                if (isDirty)
                {
                    if (test)
                    {
                        logEntry.Result = (isNew ? Monitoring.LogEntry.EntryResult.WouldBeCreated : Monitoring.LogEntry.EntryResult.WouldBeUpdated);
                    }
                    else
                    {
                        cts.Save(ct);
                        logEntry.Result = (isNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.Updated);
                    }
                }
            }

            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }
            Monitoring.Log(logEntry);
            return ct;
        }

        private static void ProcessProperties(ContentType ct, PropertyGroup tab, IEnumerable<XElement> propElements, ref bool isDirty, Monitoring.LogEntry logEntry, bool test)
        {
            var sortorder = 0;
            foreach (var propElement in propElements)
            {
                sortorder++;
                Monitoring.LogEntry subLog = logEntry.NewSubEntry(Monitoring.LogEntry.EntryAction.CheckForChanges, Monitoring.LogEntry.EntryEntity.DocumentTypeProperty, propElement.Name.LocalName);
                CreateOrUpdateProperty(ct, tab, propElement, sortorder, ref isDirty, subLog, test);
            }
        }

        private static void CreateOrUpdateProperty(ContentType ct, PropertyGroup tab, XElement propElement, int sortorder, ref bool isDirty, Monitoring.LogEntry logEntry, bool test)
        {
            try
            {
                //Check whether we are allowesd to create or update
                var autoCreate = bool.Parse(propElement.GetValue("autoCreate") ?? Settings.DocTypes.AutoCreateProperties.ToString());
                var autoUpdate = bool.Parse(propElement.GetValue("autoUpdate") ?? Settings.DocTypes.AutoUpdateProperties.ToString());
                if (!autoCreate && !autoUpdate) { return; }

                //Get the alias of the property
                var alias = propElement.Name.LocalName;
                alias = char.ToLowerInvariant(alias[0]) + alias.Substring(1);
                var name = propElement.GetValue("name") ?? (char.ToUpperInvariant(alias[0]) + alias.Substring(1));

                //Get the datatype
                var dts = ApplicationContext.Current.Services.DataTypeService;
                var dtdName = propElement.GetValue("type") ?? "Textstring";
                var allDtd = dts.GetAllDataTypeDefinitions();
                var dtd = allDtd.FirstOrDefault(d => d.Name.ToLower() == dtdName.ToLower());
                if (dtd == null)
                {
                    dtd = allDtd.FirstOrDefault(d => d.Name.ToLower() == "textstring");
                }

                //Find the property
                PropertyType pt = ct.PropertyTypes.FirstOrDefault(p => p.Alias.ToLower() == alias.ToLower());
                //If is does exists in the composition set: skip 
                if (pt == null && ct.CompositionPropertyTypes.FirstOrDefault(p => p.Alias.ToLower() == alias.ToLower()) != null)
                {
                    return;
                }


                var propIsNew = false;
                var propIsDirty = false;

                if (pt == null && autoCreate)
                {
                    pt = new PropertyType(dtd);
                    bool ok = false;
                    if (tab == null)
                    {
                        ok = ct.AddPropertyType(pt);
                    }
                    else
                    {
                        ok = ct.AddPropertyType(pt, tab.Name);
                    }
                    if (!ok) { return; }
                    propIsNew = true;
                    propIsDirty = true;
                }
                else if (autoUpdate)
                {
                    var curGroup = ct.PropertyGroups.FirstOrDefault(g => g.PropertyTypes.Any(t => t.Id == pt.Id));
                    if ((curGroup == null && tab != null) || (curGroup != null && tab == null) || (curGroup != null && tab != null && curGroup.Id != tab.Id))
                    {
                        if (tab == null)
                        {
                            if (test)
                            {
                                logEntry.Result = Monitoring.LogEntry.EntryResult.WouldBeUpdated;
                            }
                            else
                            {
                                var cts = ApplicationContext.Current.Services.ContentTypeService;
                                cts.Save(ct);
                                var oldPt = new umbraco.cms.businesslogic.propertytype.PropertyType(pt.Id);
                                oldPt.PropertyTypeGroup = 0;
                                oldPt.Save();
                                logEntry.Result = Monitoring.LogEntry.EntryResult.Updated;
                                ct = cts.GetContentType(ct.Id) as ContentType;
                            }
                        }
                        else
                        {
                            ct.MovePropertyType(pt.Alias, (tab == null ? string.Empty : tab.Name));
                            propIsDirty = true;
                        }
                    }
                }

                //Set the rest of the values
                if ((propIsNew && autoCreate) || (pt != null && autoUpdate))
                {
                    pt.SetIfDifferent(() => pt.Alias, alias, ref propIsDirty, test);
                    pt.SetIfDifferent(() => pt.DataTypeDefinitionId, dtd.Id, ref propIsDirty, test);
                    pt.SetIfDifferent(() => pt.Name, name, ref propIsDirty, test);
                    pt.SetIfDifferent(() => pt.Mandatory, bool.Parse(propElement.GetValue("mandatory") ?? "false"), ref  propIsDirty, test);
                    pt.SetIfDifferent(() => pt.Description, propElement.GetValue("description") ?? string.Empty, ref propIsDirty, test);
                    pt.SetIfDifferent(() => pt.SortOrder, int.Parse(propElement.GetValue("sort") ?? sortorder.ToString()), ref propIsDirty, test);
                    pt.SetIfDifferent(() => pt.ValidationRegExp, propElement.GetValue("validation") ?? string.Empty, ref propIsDirty, test);
                }

                if (propIsDirty)
                {
                    if (test)
                    {
                        logEntry.Result = (propIsNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.WouldBeUpdated);
                    }
                    else
                    {
                        logEntry.Result = (propIsNew ? Monitoring.LogEntry.EntryResult.Created : Monitoring.LogEntry.EntryResult.Updated);
                    }
                }

                isDirty = isDirty | propIsDirty;
            }
            catch (Exception ex)
            {
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
                logEntry.Exception = ex;
            }
        }

        public static void Delete(string initiator, Monitoring.LogEntry.EntryReason reason, bool test)
        {
            //Get all contenttypes and see if their doctypes still exist
            var cts = ApplicationContext.Current.Services.ContentTypeService;
            var allCts = cts.GetAllContentTypes();
            foreach (var ct in allCts)
            {
                var alias = ct.Alias;
                var files = Directory.GetFiles(_baseDir, alias + ".xml", SearchOption.AllDirectories);
                if (files == null || files.Length == 0)
                {
                    var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.CheckForDeletion, Monitoring.LogEntry.EntryEntity.DocumentType, alias);
                    Worker.Queue(alias, new Worker.ProcessFileDelegate(DeleteDocType), logEntry, test);
                }
            }
        }

        private static object DeleteDocType(string file, Monitoring.LogEntry logEntry, bool test)
        {
            try
            {
                var docTypeFile = Path.GetFileName(file);
                var alias = Path.GetFileNameWithoutExtension(docTypeFile);
                if (alias.Contains(' ')) { return null; }
                var cts = ApplicationContext.Current.Services.ContentTypeService;
                var ct = cts.GetContentType(alias);
                if (ct != null)
                {
                    logEntry.Result = Monitoring.LogEntry.EntryResult.NotInSolution;
                }
            }
            catch (Exception ex)
            {
                logEntry.Exception = ex;
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
            }
            Monitoring.Log(logEntry);
            return null;
        }

        public static void Export(string initiator, Monitoring.LogEntry.EntryReason reason)
        {
            //Get all documenttypes
            var cts = ApplicationContext.Current.Services.ContentTypeService;
            var contentTypes = cts.GetAllContentTypes();

            //Process them all
            foreach (var ct in contentTypes)
            {
                var logEntry = new Monitoring.LogEntry(initiator, reason, Monitoring.LogEntry.EntryAction.Export, Monitoring.LogEntry.EntryEntity.DocumentType, ct.Alias);
                Worker.Queue(ct.Alias, new Worker.ProcessFileDelegate(ExportContentType), logEntry, false);
            }
        }

        private static string ExportContentType(string alias, Monitoring.LogEntry logEntry, bool test)
        {
            string file = null;
            try
            {
                //Get contentType 
                var cts = ApplicationContext.Current.Services.ContentTypeService;
                var contentTypes = cts.GetAllContentTypes();
                var ct = cts.GetContentType(alias);

                ////Get all files (if fold exists)
                var dir = _baseDir.Replace("DocTypes", "Export\\DocTypes");

                //Create a new xDoc
                var xDoc = new XDocument(
                    new XElement(alias,
                        (ct.ParentId != -1 ? new XAttribute("master", (cts.GetContentType(ct.ParentId).Alias)) : null),
                        (ct.Name != alias ? new XAttribute("name", ct.Name) : null),
                        (!string.IsNullOrEmpty(ct.Description) ? new XAttribute("description", ct.Description ?? string.Empty) : null),
                        (!string.IsNullOrEmpty(ct.Icon) && !IsDefaultIcon(alias, ct.Icon) ? new XAttribute("icon", ct.Icon) : null),
                        (!string.IsNullOrEmpty(ct.Thumbnail) && !IsDefaultThumb(alias, ct.Thumbnail) ? new XAttribute("thumb", ct.Thumbnail) : null),
                        (ct.AllowedTemplates.Count() != 0 ? new XAttribute("templates", string.Join(",", ct.AllowedTemplates.Select(t => t.Alias))) : null),
                        (contentTypes.Any(d => d.AllowedContentTypes.Any(act => act.Id.Value == ct.Id)) ? new XAttribute("allowUnder", string.Join(",", contentTypes.Where(d => d.AllowedContentTypes.Any(cct => cct.Id.Value == ct.Id)).Select(d => d.Alias))) : null),
                        (ct.CompositionAliases().Count() != 0 ? new XAttribute("compositions", string.Join(",", ct.CompositionAliases())) : null),
                        ct.PropertyGroups.Where(pg => pg.PropertyTypes.Count != 0).OrderBy(pg => pg.SortOrder).Select(pg => new XElement(pg.Name.ToSafeAlias(),
                            new XAttribute("sort", pg.SortOrder),
                            ExportProperties(pg)
                            )),
                        ct.PropertyTypes.Count() > ct.PropertyGroups.Sum(pg => pg.PropertyTypes.Count) ? new XElement("Generic",
                            ExportProperties(ct.PropertyTypes.Except(ct.PropertyGroups.SelectMany(pg => pg.PropertyTypes))
                            )) : null
                        )
                    );

                //Save it to file
                Directory.CreateDirectory(dir);
                file = Path.Combine(dir, alias) + ".xml";
                if (test)
                {
                    logEntry.Result = Monitoring.LogEntry.EntryResult.WouldBeExported;
                }
                else
                {
                    xDoc.Save(file);
                    logEntry.Result = Monitoring.LogEntry.EntryResult.Exported;
                }
            }
            catch (Exception ex)
            {
                logEntry.Result = Monitoring.LogEntry.EntryResult.Error;
                logEntry.Exception = ex;
            }
            Monitoring.Log(logEntry);
            return file;
        }

        private static IEnumerable<XElement> ExportProperties(PropertyGroup tab)
        {
            return ExportProperties(tab.PropertyTypes);
        }

        private static IEnumerable<XElement> ExportProperties(IEnumerable<PropertyType> propertytypes)
        {
            var dts = ApplicationContext.Current.Services.DataTypeService;
            return propertytypes.OrderBy(pt => pt.SortOrder).Select(pt => new XElement(pt.Alias.ToFirstUpper(),
                            (pt.Name.ToLower() != pt.Alias.ToLower() ? new XAttribute("name", pt.Name) : null),
                            (!string.IsNullOrEmpty(pt.Description) ? new XAttribute("description", pt.Description) : null),
                            ((dts.GetDataTypeDefinitionById(pt.DataTypeDefinitionId)).Name.ToLower() != "textstring" ? new XAttribute("type", dts.GetDataTypeDefinitionById(pt.DataTypeDefinitionId).Name) : null),
                            (pt.Mandatory ? new XAttribute("mandatory", pt.Mandatory) : null),
                            (!string.IsNullOrEmpty(pt.ValidationRegExp) ? new XAttribute("validation", pt.ValidationRegExp) : null)));
        }

        private static bool IsDefaultIcon(string alias, string icon)
        {
            alias = alias.ToLower();
            icon = icon.ToLower();
            if (alias.Contains("folder") && icon == "folder.gif") { return true; }
            if (alias.Contains("data") && icon == "mediafile.gif") { return true; }
            if (alias.Contains("page") && icon == "docpic.gif") { return true; }
            return false;
        }

        private static bool IsDefaultThumb(string alias, string thumb)
        {
            alias = alias.ToLower();
            thumb = thumb.ToLower();
            if (alias.Contains("folder") && thumb == "folder.png") { return true; }
            if (alias.Contains("data") && thumb == "mediafile.png") { return true; }
            if (alias.Contains("page") && thumb == "docwithimage.png") { return true; }
            return false;
        }

    }





}