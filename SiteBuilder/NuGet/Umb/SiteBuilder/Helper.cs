﻿using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Xml;

namespace TamTam.NuGet.Umb.SiteBuilder
{
    public static class Helper
    {
        private static string _logFile = HostingEnvironment.MapPath("/app_data/sitebuilder/log.txt");
        private static string _errorsFile = HostingEnvironment.MapPath("/app_data/sitebuilder/errors.txt");

        public static string GetTempFile(string file)
        {
            var basePath = HostingEnvironment.MapPath("/");
            var relPath = file.Replace(basePath, string.Empty);
            var tempFile = HostingEnvironment.MapPath("/App_Data/SiteBuilder") + "\\" + relPath;
            var dir = Path.GetDirectoryName(tempFile);
            if (!System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.CreateDirectory(dir);
            }
            System.IO.File.Copy(file, tempFile, true);
            return tempFile;
        }

        public static void ClearCaches()
        {
            umbraco.cms.businesslogic.cache.Cache.ClearAllCache();
            var x = new Umbraco.Web.Cache.ContentTypeCacheRefresher();
            x.RefreshAll();
        }

        public static string ReadConfigSetting(string file, string xPath)
        {
            var path = Path.Combine(HttpRuntime.AppDomainAppPath, file);
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            var elm = xmlDoc.SelectSingleNode(xPath);
            if (elm != null)
            {
                return elm.InnerText;
            }
            return null;
        }


    }
}