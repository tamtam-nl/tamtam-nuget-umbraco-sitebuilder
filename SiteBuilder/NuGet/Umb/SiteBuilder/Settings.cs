﻿using System.IO;
using System.Web;
using System.Xml.Linq;

namespace TamTam.NuGet.Umb.SiteBuilder.Settings
{

    public static class Macros
    {
        public static bool Enabled = false;
        public static bool AutoCreate = false;
        public static bool AutoDelete = false;
        public static bool AutoUpdate = false;
        public static int CacheTime = 3600;
        public static bool CachePerPage = true;
        public static bool CachePerMember = true;
        public static bool UseInEditor = true;
        public static bool RenderInEditor = true;
        public static bool MonitorFiles = false;
        public static bool RunOnStartup = false;

        static Macros()
        {
            var file = Path.Combine(HttpRuntime.AppDomainAppPath, "config\\TamTam.Nuget.Umb.SiteBuilder.config");
            ReadSettings(Path.Combine(file));
        }

        public static void ReadSettings(string file)
        {
            //Get the configuration file and read the settings
            var tempFile = Helper.GetTempFile(file);
            var settings = XDocument.Load(tempFile).Element("settings").Element("macros");

            //Read the standard values from the file
            Enabled = bool.Parse(settings.GetValue("enabled") ?? "false");
            AutoCreate = bool.Parse(settings.GetValue("autoCreate") ?? "false");
            AutoDelete = bool.Parse(settings.GetValue("autoDelete") ?? "false");
            AutoUpdate = bool.Parse(settings.GetValue("autoUpdate") ?? "false");
            CacheTime = int.Parse(settings.GetValue("cacheTime") ?? "3600");
            CachePerPage = bool.Parse(settings.GetValue("cachePerPage") ?? "false");
            CachePerMember = bool.Parse(settings.GetValue("cachePerMember") ?? "false");
            UseInEditor = bool.Parse(settings.GetValue("useInEditor") ?? "false");
            RenderInEditor = bool.Parse(settings.GetValue("renderInEditor") ?? "false");
            MonitorFiles = bool.Parse(settings.GetValue("monitorFiles") ?? "false");
            RunOnStartup = bool.Parse(settings.GetValue("runOnStartup") ?? "false");
        }
    }

    public static class Templates
    {
        public static bool Enabled = false;
        public static bool AutoCreate = false;
        public static bool AutoDelete = false;
        public static bool AutoUpdate = false;
        public static bool MonitorFiles = true;
        public static bool AutoLinkToDocType = true;
        public static bool RunOnStartup = false;

        static Templates()
        {
            var file = Path.Combine(HttpRuntime.AppDomainAppPath, "config\\TamTam.Nuget.Umb.SiteBuilder.config");
            ReadSettings(file);
        }

        public static void ReadSettings(string file)
        {
            //Get the configuration file and read the settings
            var settings = XDocument.Load(file).Element("settings").Element("templates");

            //Read the standard values from the file
            Enabled = bool.Parse(settings.GetValue("enabled") ?? "false");
            AutoCreate = bool.Parse(settings.GetValue("autoCreate") ?? "false");
            AutoDelete = bool.Parse(settings.GetValue("autoDelete") ?? "false");
            AutoUpdate = bool.Parse(settings.GetValue("autoUpdate") ?? "false");
            MonitorFiles = bool.Parse(settings.GetValue("monitorFiles") ?? "false");
            AutoLinkToDocType = bool.Parse(settings.GetValue("autoLinkToDocType") ?? "false");
            RunOnStartup = bool.Parse(settings.GetValue("runOnStartup") ?? "false");
        }
    }

    public static class DocTypes
    {
        public static bool Enabled = false;
        public static bool AutoCreateDocTypes = false;
        public static bool AutoUpdateDocTypes = false;
        public static bool AutoCreateProperties = false;
        public static bool AutoUpdateProperties = false;
        public static bool MonitorFiles = true;
        public static bool RunOnStartup = false;

        static DocTypes()
        {
            var file = Path.Combine(HttpRuntime.AppDomainAppPath, "config\\TamTam.Nuget.Umb.SiteBuilder.config");
            ReadSettings(file);
        }

        public static void ReadSettings(string file)
        {
            //Get the configuration file and read the settings
            var settings = XDocument.Load(file).Element("settings").Element("docTypes");

            //Read the standard values from the file
            Enabled = bool.Parse(settings.GetValue("enabled") ?? "false");
            AutoCreateDocTypes = bool.Parse(settings.GetValue("autoCreateDocTypes") ?? "false");
            AutoUpdateDocTypes = bool.Parse(settings.GetValue("autoUpdateDocTypes") ?? "false");
            AutoCreateProperties = bool.Parse(settings.GetValue("autoCreateProperties") ?? "false");
            AutoUpdateProperties = bool.Parse(settings.GetValue("autoUpdateProperties") ?? "false");
            MonitorFiles = bool.Parse(settings.GetValue("monitorFiles") ?? "false");
            RunOnStartup = bool.Parse(settings.GetValue("runOnStartup") ?? "false");

        }
    }

    public static class DataTypes
    {
        public static bool Enabled = false;
        public static bool AutoCreateDataTypes = false;
        public static bool AutoUpdateDataTypes = false;
        public static bool MonitorFiles = true;
        public static bool RunOnStartup = false;

        static DataTypes()
        {
            var file = Path.Combine(HttpRuntime.AppDomainAppPath, "config\\TamTam.Nuget.Umb.SiteBuilder.config");
            ReadSettings(file);
        }

        public static void ReadSettings(string file)
        {
            //Get the configuration file and read the settings
            var settings = XDocument.Load(file).Element("settings").Element("dataTypes");

            //Read the standard values from the file
            Enabled = bool.Parse(settings.GetValue("enabled") ?? "false");
            AutoCreateDataTypes = bool.Parse(settings.GetValue("autoCreateDataTypes") ?? "false");
            AutoUpdateDataTypes = bool.Parse(settings.GetValue("autoUpdateDataTypes") ?? "false");
            MonitorFiles = bool.Parse(settings.GetValue("monitorFiles") ?? "false");
            RunOnStartup = bool.Parse(settings.GetValue("runOnStartup") ?? "false");

        }
    }

}
