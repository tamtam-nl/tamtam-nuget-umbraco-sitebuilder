﻿@echo Off
set config=%1
if "%config%" == "" (
   set config=Release
)

if not "%PackageVersion%" == "" (
	set PackageVersion=%PackageVersion:1.0.0=1.0.0%
)
set version=-Version %PackageVersion%

REM Build
%WINDIR%\Microsoft.NET\Framework\v4.0.30319\msbuild SiteBuilder.sln /p:Configuration="%config%" /m /v:M /fl /flp:LogFile=msbuild.log;Verbosity=Normal /nr:false

REM Package
mkdir Build
cmd /c %nuget% pack "SiteBuilder\SiteBuilder.nuspec" -o Build -p Configuration=%config% %version%
REM cmd /c %nuget% pack "SiteBuilder\SiteBuilder.Binary.nuspec" -o Build -p Configuration=%config% %version%