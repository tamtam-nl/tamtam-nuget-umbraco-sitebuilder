#UMBRACO SITEBUILDER
## Intro
This package creates, updates and deletes Document types (delete not available), templates and macros based on the files in your solution. It will update your Umbraco configuration by monitoring your files, on application startup or manually. All can be easily configured and using transforms you can use different settings for different environments (using SlowCheetah).

TT SiteBuilder is not a code-first solution like uSitebuilder or similar solutions. Where these use reflection to create the entities in Umbraco, TT Sitebuilder looks at the files and XML declarations of document types to manage the entities. This allows it to be very fast and not cause any application starts with every property or setting that is changed. To accomodate for existing projects it also has an export function that creates the XML declartions for you so you can easily incorporate this in any existing website.

The concept is based on the idea that only relevant information needs to be provided so you don't need to provide any inforation that is obvious. For example:

* if there is a view with the samename as the document type it will automatically attach it.
* If there is an icon with the same name as the document type is will automatically use it
* If a property is a text string you do not have to provide the type

**The current V7.V2 version only works for Umbraco 7.2.3+. Packages for earlier versions of Umbraco can be found on MyGet**

## Package
The package installs the following:

* A configuration file in the Config folder.
* A dll in case of the binary package.
* Several classes under Nuget\Umb\SiteBuilder in case of the -code- package
* A Manager.html file
* Several sample txt files to explain how to tell Sitebuilder what to do
* It adds a line to the handlers sections in the web.config

## Configuration
The configuration is separated in three sections: macros, templates and doctypes. Each containing a set of attributes to determine the workings.

###Available in all 3 sections
| Attribute | Description
|---|---
| enabled|Determines if sitebuilder needs to process anything for that section.
| monitorFiles|When set to true, Sitebuilder will be process changes when they are saved to file, when a file is renamed or when a file is deleted.
| runOnStartup|When set to true, Sitebuilder will scan all files for that section when the application is started. ''(Not recommended)''

*It is recommended to use file monitoring on your development environment and to use manual processing on your production environment*

###macros
| Attribute | Description
|---|---
| autoCreate|When set to true, macro files that have no macro attached in Umbraco are automatically added when a macropartial is added to the solution, including settings and parameters
| autoUpdate|When set to true, macros in Umbraco are updated with the new information from the file when it is saved.
| autoDelete|When set to true, macros are automatically deleted when the file it links to can no longer be found. When set to false, the manager will show suggestions for deletion.
| cacheTime|Sets the cachetime property for all macros if not overruled in the macro file itself
| cachePerPage|Sets the CachePerPage property for all macros if not overruled in the macro file itself
| cachePerMember|Sets the CachePerMember property for all macros if not overruled in the macro file itself
| useInEditor|Sets the useInEditor property for all macros if not overruled in the macro file itself
| renderInEditor|Sets the renderInEditorproperty for all macros if not overruled in the macro file itself

###templates
| Attribute | Description
|---|--
| autoCreate|When set to true, templates are automatically created when new views are added to the solution.
| autoUpdate|When set to true, templates are automatically updated when a view is changed in the solution.
| autoDelete|When set to true, templates are automatically deleted when a view is changed in the solution. When set to false, the manager will show suggestions for deletion.
| autoLinkToDocType|When set to true, document types with the same name as the template should automacally be set to use that template as default.

#docTypes
| Attribute | Description
|---|---
| autoCreateDocTypes|When set to true, document types and tabs are automatically created when a new xml definition file  is added to the solution
| autoUpdateDocTypes|When set to true, document types, tabs are automatically update when an xml definition file  is modified in the solution
| autoCreateProperties|When set to true, document type properties are automatically created when a property is added to the xml definition file.
| autoUpdateProperties|When set to true, document type properties are automatically updated when a property changes in an xml definition file.

## Macros
Macros values are set using the default settings from the configuration file or by overruling these settings in the macro file itself. This is done by adding the following snippet to the top of the macropartial cshtml file. Only values that need to be overruled have to be added. You can and should leave out all other lines.

*By default all macros will be created with an alias and name based on the location of the file in the tree. If this is ok do not set it explicitly in the file*

	<source lang="csharp">
	@*SiteBuilder
	AutoCreate=true
	AutoUpdate=true
	Alias=SampleMacro
	Name=My Sample Macro
	CacheTime=600
	CachePerPage=true
	CachePerMember=true
	UseInEditor=true
	RenderInEditor=true
	Property=prop1,contentPicker,Property 1
	Property=prop2
	Property=prop3,number
	*@
	</source>

###Extra settings

Most settings are the same as the ones in the configuration except for the following

| Setting | Description
|---|---
| Alias|When a different alias is required then the file name it can be overruled here.
| Name|When a name is needed that differs from the alias is can be overruled here.
| Property|You can add properties by setting the alias, type and name in a comma separated string. If the type is text is does not have to be set. If the name is equal to the alias it does not have to be set.

## Templates
Templates are automatically set with an alias and name equal to the filename of the view. When using Layout=[xxx] statements in your view, the template is automatically added as a child of the template that the Layout refers to. As long as it exists. By using the following snippet the name can be overruled.


	<source lang="csharp">
	@*SITEBUILDER
	name=Sample page
	*@
	</source>

## Document Types
Document types are build using XML definition files which are stored in the SiteBuilder folder under the root of the website. There is no specific structure in how to manage them, since all folders and sub folders are scanned. 

**Only information that differs from the default workings of sitebuilder need to be set. e.g. if the icon file name of the document type is the same as the alias of the document type you do not have to set it.**

The Xml definition is build up in 3 levels: 

* Level 1 is the document type itself containing attributes for the settings of the document type. 
* Level 2 are the tabs of the document type in the order required.
* Level 3 are the properties within a specific tab the order required with attributes for the property settings.

This is how the XML looks like:

	<source lang="xml">
	<?xml version="1.0" encoding="utf-8" ?>
	<SamplePage name="Sample Page" allowAtRoot="true" description="My sample page" icon="member.gif" thumb="member.png" master="BaseSample">
	  <Sample_Tab>
	    <sampleProperty name="Sample Property" description="My sample property" mandatory="true" validation="\d" type="Textbox Multiple" />
	  </Sample_Tab>
	</SamplePage>
	</source>

###Document Type Attributes
*The name of the element has to be equal to the name of the file and sets the alias of the document type.*

| Setting | Description
|---|---
| name|Sets the name of the document type. If not there the alias is used. 
| allowAtRoot|True to allow at root. In not there defaults to false.
| templates|Comma delimited names of the templates allowed for this document type. If not there defaults to template that has same name as document type.
| description|Sets the description of the document type.
| icon|Sets the icon file name. If not there defaults to image that has the same name as the document type.
| thumb|Sets the thumbnail file name. If not there defaults to image that has the same name as the document type.
| compositions|Comma delimited names of the composition templates to attach.

###Tab Attributes
*The name of the element sets the name of the tab. Use underscores for spaces and use 'Generic' to set properties for the Generic Properties tab.*

| Setting | Description
|---|---
| sort|Sets the sort index of the tab. If not there defaults to the location in the xml.

###Property Attributes
*The name of the element sets the alias of the property. The first letter is automatically made lower case*

| Setting | Description
|---|---
| name|Sets the name (caption). Defaults to the alias if not there.
| type|Sets the data type as declared in Umbraco. Default to 'textstring' if not there.
| mandatory|sets whether the property is mandatory. Default to false if not there.
| description|Sets the description.
| validation|Sets the validation.

## Export
To run the export use the manager (see next part). When export is activated all document types are exported to Xml definition files in the \SiteBuilder\Export folder. From there file can be copied to the \SiteBuilder\DocTypes folder to be picked up by the import process.

## Manager
To open the manager go to /sitebuilder/manager.html. From there you can view and clean the log, start the import or run the export.